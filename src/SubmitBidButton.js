import React, { useEffect, useState } from "react";
import ReactTooltip from "react-tooltip";
import { useSubstrate } from "./substrate-lib";
const queryResHandler = (result) =>
  result.isNone ? console.log("Nothing") : console.log(result.toString());
const getFromAcct = async (accountAddress) => {
  const {
    address,
    meta: { source, isInjected },
  } = accountAddress;
  let fromAcct;

  // signer is from Polkadot-js browser extension
  if (isInjected) {
    const injected = await web3FromSource(source);
    fromAcct = address;
    api.setSigner(injected.signer);
  } else {
    fromAcct = accountAddress;
  }

  return fromAcct;
};
export default function SubmitButton({ accountAddress, type }) {
  const { api } = useSubstrate();
  return (
    <button
      onClick={async () => {
        let tx = [];
        let fromAcct = await getFromAcct(accountAddress);
        let result =
          type === "bid"
            ? await api.tx.dexter
                .placeBidOrder({ marketorder: "Dot" })
                .signAndSend(fromAcct)
            : await api.tx.dexter
                .placeAskOrder({ marketorder: "Dot" })
                .signAndSend(fromAcct);

        queryResHandler(result);
      }}
    >
      Click me
    </button>
  );
}
