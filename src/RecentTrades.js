import { Card, GridRow, Icon, Table } from "semantic-ui-react";
import React, { useEffect, useState } from 'react';
import { Feed, Grid, Button } from 'semantic-ui-react';

import { useSubstrate } from './substrate-lib';
import { element } from "prop-types";
const { ApiPromise } = require('@polkadot/api');
// Events to be filtered from feed
const FILTERED_EVENTS = [
];

function Main (props) {
  const { api } = useSubstrate();
  const [eventFeed, setEventFeed] = useState([]);
  let count = 0; 
  let elements = []
  useEffect(() => {
    const getBidOrders = async () => {
      try {
        if( api ){
          let res = await api.rpc.dexter.getBidOrders(10);
          let local = await res.map(element => {
            count++;
            return <h3 key={count}>{element.toHuman()}</h3>
          });
          count = 0;
          setEventFeed([local]);
        }
      } catch (e) {
        //console.error(e);
      }
    };
    const getAskOrders = async () => {
      try {
        if( api ){
          let res = await api.rpc.dexter.getAskOrders(10);
          let local = await res.map(element => {
            count++;
            return <Grid.Row key={count}>{element.toHuman()}</Grid.Row>
          });
          count = 0;
          setEventFeed([local]);
        }
      } catch (e) {
        //console.error(e);
      }
    };
    setInterval(() => {
      props.type==="ask"?getAskOrders():getBidOrders()
    }, 5000);
  }, [api]);

  const { feedMaxHeight = 250 } = props;

  return (
    <Card fluid>
      <Card.Header>
      <h1 style={{ float: 'left' }}>Recent {props.type} Orders</h1></Card.Header>
      <Card.Content width={8}>{eventFeed}</Card.Content>
      <Button
        basic circular
        size='mini'
        color='grey'
        floated='right'
        icon='erase'
        onClick={ _ => setEventFeed([]) }
      />
    </Card>
  );
}

export default function RecentTrades (props) {
  const { api } = useSubstrate();
  return api.query && api.query.system && api.query.system.events
    ? <Main {...props} />
    : null;
}

// const RecentTrades = () => (
//     <>
//       <h1>Recent Trades</h1>
//       <Table celled selectable>
//         <Table.Header>
//           <Table.Row>
//             <Table.HeaderCell>Price</Table.HeaderCell>
//             <Table.HeaderCell>Amount</Table.HeaderCell>
//             <Table.HeaderCell>Time</Table.HeaderCell>
//           </Table.Row>
//         </Table.Header>
  
//         <Table.Body>
//           <Table.Row>
//             <Table.Cell>No Name Specified</Table.Cell>
//             <Table.Cell>Unknown</Table.Cell>
//             <Table.Cell negative>None</Table.Cell>
//           </Table.Row>
//           <Table.Row positive>
//             <Table.Cell>Jimmy</Table.Cell>
//             <Table.Cell>
//               <Icon name="checkmark" />
//               Approved
//             </Table.Cell>
//             <Table.Cell>None</Table.Cell>
//           </Table.Row>
//           <Table.Row>
//             <Table.Cell>Jamie</Table.Cell>
//             <Table.Cell>Unknown</Table.Cell>
//             <Table.Cell positive>
//               <Icon name="close" />
//               Requires call
//             </Table.Cell>
//           </Table.Row>
//           <Table.Row negative>
//             <Table.Cell>Jill</Table.Cell>
//             <Table.Cell>Unknown</Table.Cell>
//             <Table.Cell>None</Table.Cell>
//           </Table.Row>
//         </Table.Body>
//       </Table>
//     </>
//   );