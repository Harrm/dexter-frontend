import React, { useState, createRef, useEffect } from "react";
import {
  Container,
  Dimmer,
  Loader,
  Grid,
  Sticky,
  Message,
  Image,
  GridColumn,
} from "semantic-ui-react";
import "semantic-ui-css/semantic.min.css";
import ReactTooltip from "react-tooltip";

import { SubstrateContextProvider, useSubstrate } from "./substrate-lib";
import { DeveloperConsole, TxButton } from "./substrate-lib/components";

import AccountSelector from "./AccountSelector";
import Balances from "./Balances";
import BlockNumber from "./BlockNumber";
import Events from "./Events";
import Interactor from "./Interactor";
import Metadata from "./Metadata";
import NodeInfo from "./NodeInfo";
import TemplateModule from "./TemplateModule";
import RecentTrades from "./RecentTrades";
import Transfer from "./Transfer";
import Upgrade from "./Upgrade";
import "./main.css";

import {
  Area,
  AreaChart,
  CartesianGrid,
  Line,
  LineChart,
  ReferenceLine,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from "recharts";
import { useWindowSize } from "react-use";
import { u32 } from "@polkadot/types";
import SubmitBidButton from "./SubmitBidButton";

function Main() {
  const [accountAddress, setAccountAddress] = useState(null);
  const { api, apiState, keyring, keyringState, apiError } = useSubstrate();
  const [bidOrders, setBidOrders] = useState({ data: null, version: null });

  const accountPair =
    accountAddress &&
    keyringState === "READY" &&
    keyring.getPair(accountAddress);

  const { width, height } = useWindowSize();

  const data = [
    { name: "Page A", uv: 1000, pv: 2400, amt: 2400, uvError: [75, 20] },
    { name: "Page B", uv: 300, pv: 4567, amt: 2400, uvError: [90, 40] },
    { name: "Page C", uv: 280, pv: 1398, amt: 2400, uvError: 40 },
    { name: "Page D", uv: 200, pv: 9800, amt: 2400, uvError: 20 },
    { name: "Page E", uv: 278, pv: null, amt: 2400, uvError: 28 },
    { name: "Page F", uv: 189, pv: 4800, amt: 2400, uvError: [90, 20] },
    { name: "Page G", uv: 189, pv: 4800, amt: 2400, uvError: [28, 40] },
    { name: "Page H", uv: 189, pv: 4800, amt: 2400, uvError: 28 },
    { name: "Page I", uv: 189, pv: 4800, amt: 2400, uvError: 28 },
    { name: "Page J", uv: 189, pv: 4800, amt: 2400, uvError: [15, 60] },
  ];
  const loader = (text) => (
    <Dimmer active>
      <Loader size="small">{text}</Loader>
    </Dimmer>
  );

  const message = (err) => (
    <Grid centered columns={2} padded>
      <Grid.Column>
        <Message
          negative
          compact
          floating
          header="Error Connecting to Substrate"
          content={`${JSON.stringify(err, null, 4)}`}
        />
      </Grid.Column>
    </Grid>
  );

  if (apiState === "ERROR") return message(apiError);
  else if (apiState !== "READY") return loader("Connecting to Substrate");

  if (keyringState !== "READY") {
    return loader(
      "Loading accounts (please review any extension's authorization)"
    );
  }

  const contextRef = createRef();

  return (
    <div ref={contextRef}>
      <Sticky context={contextRef}>
        <AccountSelector setAccountAddress={setAccountAddress} />
      </Sticky>
      <Container>
        <Grid stackable columns="equal">
          <Grid.Row stretched style={{ borderBottom: "1px solid #cccccc" }}>
            <ResponsiveContainer width={"100%"} height={500}>
              <AreaChart
                data={data}
                margin={{ top: 20, right: 30, left: 0, bottom: 0 }}
              >
                <XAxis dataKey="name" />
                <YAxis />
                <CartesianGrid strokeDasharray="3 3" />
                <Tooltip />
                <ReferenceLine x="Page C" stroke="green" label="Min PAGE" />
                <ReferenceLine
                  y={4000}
                  label="Max"
                  stroke="red"
                  strokeDasharray="3 3"
                />
                <Area
                  type="monotone"
                  dataKey="uv"
                  stroke="#8884d8"
                  fill="#8884d8"
                />
              </AreaChart>
            </ResponsiveContainer>
          </Grid.Row>
          <Grid.Row
            columns="equal"
            stretched
            style={{ borderBottom: "1px solid #cccccc" }}
          >
            <NodeInfo />
            <ReactTooltip id="more-info" className="custom-tooltip">
              <Image
                src={`${process.env.PUBLIC_URL}/assets/substrate-logo.png`}
                size="mini"
              />
              <h2>Substrate node info</h2>
            </ReactTooltip>
            <Metadata />
            <BlockNumber />
            <BlockNumber finalized />
          </Grid.Row>
          <Grid.Row>
            <GridColumn stretched>
              <RecentTrades type={"bid"}></RecentTrades>
              <SubmitBidButton accountAddress={accountPair} type="bid"/>
            </GridColumn>
            <GridColumn stretched>
              <RecentTrades type={"ask"}></RecentTrades>
              <SubmitBidButton accountAddress={accountPair} type="ask"/>
            </GridColumn>
          </Grid.Row>
          <Grid.Row>
            <Interactor accountPair={accountPair} />
            {/* <Events /> */}
          </Grid.Row>
          <Grid.Row>
            <TemplateModule accountPair={accountPair} />
          </Grid.Row>
        </Grid>
      </Container>
      <DeveloperConsole />
    </div>
  );
}

export default function App() {
  return (
    <SubstrateContextProvider>
      <Main />
    </SubstrateContextProvider>
  );
}
